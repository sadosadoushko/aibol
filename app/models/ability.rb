# frozen_string_literal: true

class Ability
  include CanCan::Ability
  def initialize(user)
    user ||= User.new # guest user (not logged in)
    if user.admin?
      can :manage, :all
    elsif user.doctor?
      can :read, :all
      can :manage, [Appointment, PatientRecord]
    elsif user.patient?
      can :read, :all
      can :create, Appointment
      can :manage, PatientRecord, user_id: user.id
    end
  end
end
