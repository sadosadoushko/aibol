class User < ApplicationRecord
  enum role: [:patient, :doctor, :admin], default:'patient'
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
end
