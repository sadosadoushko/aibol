Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users

  authenticated :user do
    root 'appointments#index', as: :authenticated_root
  end

  root 'welcome#index'

  namespace :admin do
    resources :users
    resources :doctors
    resources :patients
  end

  resources :appointments
  resources :patient_records

  # Admin-only routes
  namespace :admin do
    root 'users#index'
    resources :users
    resources :doctors
    resources :patients
  end

  # Doctor-only routes
  resources :appointments, only: [:index, :show]
  resources :patient_records, only: [:index, :show]

  # Patient-only routes
  resources :appointments, only: [:new, :create]
  resources :patient_records, only: [:new, :create, :edit, :update]
end
